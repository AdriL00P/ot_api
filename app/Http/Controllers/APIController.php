<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\App;

class APIController extends BaseController
{

    public function categorias()
    {
        $baseUrl = env('API_ENDPOINT');
        $client  = new Client(); //GuzzleHttp\Client

        // Endpoint específico a consumir
        $url = $baseUrl . "/categories";

        //$headers = ['api-key' => ''];

        try {
            $response = $client->request('GET', $url, [
                'verify' => false,
                //'headers' => $headers,
            ]);
        } catch (\Exception $e) {
            $status_code = $e->getResponse()->getStatusCode();
            return response()->json([
                'error'    => $status_code,
                'message'  => $e->getMessage(),
                'response' => $e->getResponse(),
            ],
                $status_code);
        }

        $responseBody = json_decode($response->getBody());

        // Parseo los resultados

        $results = [];
        foreach ($responseBody as $key => $value) {
            array_push($results, [
                'id'     => $key,
                'nombre' => $value,
                'url'    => env('APP_URL') . '/api/categories/' . strtolower($value),
            ]);
        }

        // Estructura general del Json
        $responseBody = [
            'total'      => count($results),
            'paginacion' => 'no',
            'datos'      => $results,
        ];
        return response()->json($responseBody);
    }

    public function categoriaPorNombre(Request $request, String $name)
    {
        $baseUrl = env('API_ENDPOINT');
        $client  = new Client(); //GuzzleHttp\Client

        // Endpoint específico a consumir
        $url = $baseUrl . "/entries";

        //$headers = ['api-key' => ''];

        try {
            $response = $client->request('GET', $url, [
                'verify' => false,
                //'headers' => $headers,
                'query'  => [
                    'category' => $name,
                    'https'    => 'true'],
            ]);

        } catch (\Exception $e) {
            $status_code = $e->getResponse()->getStatusCode();
            return response()->json([
                'error'    => $status_code,
                'message'  => $e->getMessage(),
                'response' => $e->getResponse(),
            ],
                $status_code);
        }

        $responseBody = json_decode($response->getBody());

        // Estructura general del Json
        $responseBody = [
            'total'      => $responseBody->count,
            'paginacion' => 'no',
            'datos'      => $responseBody->entries,
        ];
        return response()->json($responseBody);
    }

}
